package com.Guess_Game;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseUser;

public class GameActivity extends ActionBarActivity {

	private EditText et2;
	private int score = 0, level = 1;
	// private TextView tv;
	private Button startbtn;
	private Button guessbtn;
	private Button btnsignout;
	private TextView lvl;
	private TextView scr;
	private TextView txtelapsed;
	protected String check;
	private TextView txtnum;
	private TextView txtWelcome;
	public ParseUser currentUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		currentUser = ParseUser.getCurrentUser();
		txtWelcome = (TextView) findViewById(R.id.textView3);
		txtnum = (TextView) findViewById(R.id.text_guessnum);
		et2 = (EditText) findViewById(R.id.editnum);
		// tv = (TextView)findViewById(R.id.textView2);
		startbtn = (Button) findViewById(R.id.btnstart);
		guessbtn = (Button) findViewById(R.id.btnguess);
		lvl = (TextView) findViewById(R.id.text_level);
		scr = (TextView) findViewById(R.id.text_score);
		txtelapsed = (TextView) findViewById(R.id.txtelapsed);

		guessbtn.setEnabled(false);
		btnsignout = (Button) findViewById(R.id.btnsignout);
		btnsignout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ParseUser.logOut();
				Toast.makeText(getBaseContext(), "Signing out",
						Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getBaseContext(),
						LoginSignupActivity.class);
				startActivity(intent);
				finish();

			}
		});
		txtWelcome.setText("Welcome " + currentUser.getUsername().toString());
		startbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String str = nextNum();
				txtnum.setText(str);
				
				Timer t = new Timer();
				t.schedule(new TimerTask() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								
								
								txtnum.setVisibility(View.INVISIBLE);
							}
						});
					}
				}, 3000);
				startbtn.setText("Next");

				String l = Integer.toString(level);
				lvl.setText(l);
				startbtn.setEnabled(false);
				guessbtn.setEnabled(true);
			}
		});

		guessbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				check = txtnum.getText().toString();
				String comp = et2.getText().toString();

				txtnum.setVisibility(View.VISIBLE);
				startbtn.setEnabled(true);
				guessbtn.setEnabled(false);
				if (comp.equalsIgnoreCase(check)) {

					Toast.makeText(getBaseContext(), "Correct Answer",
							Toast.LENGTH_LONG).show();
					score = (score + (5 * level));
					String s = Integer.toString(score);
					scr.setText(s);
					level++;
					// /////////////////////////////////////////////
					ParseObject gameScore = new ParseObject("GameScore");
					gameScore.put("score", score);
					gameScore.put("level", level);
					gameScore.put("userId", currentUser.getObjectId()
							.toString());
					gameScore.put("Username", currentUser.getUsername()
							.toString());

					gameScore.saveInBackground();

				} else {
					Toast.makeText(getBaseContext(), "Ohh! You lose",
							Toast.LENGTH_LONG).show();
					ScoreBoard();

				}
				et2.setText("");
				txtnum.setText("");
			}
		});

	}

	public void dialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("Welcome");

		// Setting Dialog Message
		alertDialog
				.setMessage("This is a very simple and interesting game. You have to memorize and type right word");

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("Play",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						// Write your code here to invoke YES event
						Toast.makeText(getApplicationContext(),
								"Click on Start Button", Toast.LENGTH_SHORT)
								.show();
						dialog.dismiss();
					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("Exit",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Write your code here to invoke NO event
						Toast.makeText(getApplicationContext(),
								"You clicked on Exit", Toast.LENGTH_SHORT)
								.show();
						finish();

					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	public void ScoreBoard() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("Game END");

		// Setting Dialog Message
		alertDialog.setMessage("Summary" + "\n Score = " + score + "\nLevel = "
				+ level);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("Restart",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						// Write your code here to invoke YES event
						Toast.makeText(getApplicationContext(),
								"Click on Start Button", Toast.LENGTH_SHORT)
								.show();
						dialog.dismiss();
					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("Exit",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Write your code here to invoke NO event
						Toast.makeText(getApplicationContext(),
								"You clicked on Exit", Toast.LENGTH_SHORT)
								.show();
						finish();

					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	private String nextNum() {
		Random r = new Random(1000);
		int num = r.nextInt(100000 * level);
		String rstr = Integer.toString(num);
		return rstr;
	}


}
