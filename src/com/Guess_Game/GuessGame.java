package com.Guess_Game;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;

public class GuessGame extends Application {
	public void onCreate() {
		
		Parse.initialize(this, "mji8Q1eimfYShjOhy7yYQcXSM5aBVcTOVk8eIOjj",
				"FZ7dSUnlNxo6zpFxbX7fD3Asfz1V0DScDUiUYxCX");
		PushService.setDefaultPushCallback(this, GameActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();

		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();


		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);
		
	}


}
